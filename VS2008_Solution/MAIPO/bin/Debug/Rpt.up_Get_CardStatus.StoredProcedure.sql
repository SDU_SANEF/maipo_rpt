USE [ITS_CSC]
GO
/****** Object:  StoredProcedure [Rpt].[up_Get_CardStatus]    Script Date: 4/2/2015 10:57:18 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[up_Get_CardStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Rpt].[up_Get_CardStatus]
GO
/****** Object:  StoredProcedure [Rpt].[up_Get_CardStatus]    Script Date: 4/2/2015 10:57:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[up_Get_CardStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Rpt].[up_Get_CardStatus] AS' 
END
GO

-- =============================================
-- Author:		Dutot Sebastien
-- Create date: 205-01-07
-- Description:	Return the list of CardStatus Description by concatenation
-- 2015-04-06 SDU: Updated Query to get "en-sl" translation
-- =============================================
ALTER PROCEDURE [Rpt].[up_Get_CardStatus]
	-- Add the parameters for the stored procedure here
	@CardStatusIDs nvarchar(max)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	DECLARe @CardStatusDesc nvarchar(max)
	DECLARe @allCardStatusDesc nvarchar(max)


	select @CardStatusDesc= 
	STUFF( (SELECT ',' + coalesce(CardStatus.Translations.value('(/rootnode/data[@name="es-cl"])[1]','nvarchar(50)'),CardStatus.CardStatusDesc)
	--CardStatusDesc
from Customer.CardStatus
where CardStatusId in (select Item from dbo.ufnSplit(@CardStatusIDs,','))
                             FOR XML PATH('')), 
                            1, 1, '')
	select @allCardStatusDesc= 
	STUFF( (SELECT ',' + coalesce(CardStatus.Translations.value('(/rootnode/data[@name="es-cl"])[1]','nvarchar(50)'),CardStatus.CardStatusDesc)
	--CardStatusDesc
from Customer.CardStatus
                             FOR XML PATH('')), 
                            1, 1, '')
IF @CardStatusDesc=@allCardStatusDesc
begin 
select 'ALL' as CardStatus_Desc
end
else
begin
select coalesce(@CardStatusDesc,'N/A') as CardStatus_Desc
end	
END



GO
