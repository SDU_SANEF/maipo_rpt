USE [ITS_CSC]
GO
/****** Object:  StoredProcedure [Rpt].[PAT Pendientes]    Script Date: 4/2/2015 10:57:18 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[PAT Pendientes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Rpt].[PAT Pendientes]
GO
/****** Object:  StoredProcedure [Rpt].[PAT Pendientes]    Script Date: 4/2/2015 10:57:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[PAT Pendientes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [Rpt].[PAT Pendientes] AS' 
END
GO



-- =============================================
-- Author:		Sebastien Dutot
-- Create date: 2015/04/02
-- Description:	Generate list of records for the Report PAT Pendientes
-- Based on Query from Report "Respuestas PatPass" with Card details and Filters on Card creation date, Card IsActive and Card Status
-- 2015-04-06 SDU : Updated Query to Get "en-sl" translation from CardStatus
-- =============================================
ALTER PROCEDURE [Rpt].[PAT Pendientes]
	-- Add the parameters for the stored procedure here
	@pStartDate datetime = NULL,
	@pEndDate datetime= NULL,
	--@IsActive nvarchar(max),
	@CardStatus nvarchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		declare @StartDate int
	declare @EndDate int
		--declare @IsActiveIds table (item nvarchar(10))
	declare @CardStatusIds table (item nvarchar(10))

	--select @StartDate = date_key from ITS_ODS.dim.Date
	--where date.Date = isnull(@pStartDate,convert(date,dateadd(day,-30,getdate())))

	--		select @EndDate  = date_key from ITS_ODS.dim.Date
	--where date.Date = isnull(@pEndDate,convert(date,dateadd(day,30,getdate())))
	select @pStartDate= convert(date,isnull(@pStartDate,convert(date,dateadd(day,-30,getdate()))))

			select @pEndDate  =convert(date,isnull(@pEndDate,convert(date,dateadd(day,30,getdate()))))


	--insert into  @IsActiveIds(Item) (select Item from dbo.ufnSplit(@IsActive,','))

	--insert @HighwayIds(Item) value (select (coalesce(cast(@Highwayid as nvarchar),(select Item from dbo.ufnSplit(@Highway,','))))
	--select @TollpointIds= coalesce(cast(@tollPointId as nvarchar),(select Item from dbo.ufnSplit(@tollPoint,',')))

	insert into  @CardStatusIds(Item) (select Item from dbo.ufnSplit(@CardStatus,','))


select 
Customer.Account.AccountNumber as 'N de Cuenta',
--Rebill.PatPassRequest.CustomerName as 'Nombre Cliente',
--Rebill.PatPassRequest.NID as 'RUT',
--Rebill.PatPassRequest.PatPassRequestStatusDate as 'Fecha Respuesta',
Card.NameOnCard as 'Nombre Cliente',
Card.NID as 'RUT',
--Card.RebillAttemptDt as 'Fecha Respuesta',
coalesce(CardStatus.Translations.value('(/rootnode/data[@name="es-cl"])[1]','nvarchar(50)'),CardStatus.CardStatusDesc) as 'Estado Pat',
--CardStatus.CardStatusDesc as 'Estado Pat',
Card.Created as 'Fecha Creacion',
--Rebill.PatPassRequest.Created as 'Fecha Creacion',
card.LastUpdApp as 'Metodo de Solicitud',
Card.UECCNum as 'N de Tarjeta',
cardtype.CreditCardDesc as 'Marca',
convert(nvarchar(2),card.ExpMonth)+'/'+convert(nvarchar(4),Card.ExpYear) as 'Fecha de Vencimiento'
--from Rebill.PatPassRequest 
--inner join Customer.Account on Customer.Account.AccountId=Rebill.PatPassRequest.AccountId
--left join customer.Card on Card.AccountId=Account.AccountId
--select *
from customer.Card
 join Customer.CardType on card.CardTypeId=CardType.CardTypeId 
 join customer.CardStatus on card.CardStatusId=CardStatus.CardStatusId
 join Customer.Account on Card.AccountId=Account.AccountId

 where convert(date,card.created) between @pStartDate and  @pEndDate
  --and Card.isActive in (select item from @IsActiveIds) --(select Item from dbo.ufnSplit(@Highway,','))
  and CardStatus.CardStatusId in (select item from  @CardStatusIds)--(select Item from dbo.ufnSplit(@TollPoint,','))
order by card.created  desc


END





GO
