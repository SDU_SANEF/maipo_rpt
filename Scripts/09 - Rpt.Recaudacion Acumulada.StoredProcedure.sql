USE [ITS_CSC]
GO
/****** Object:  StoredProcedure [Rpt].[Recaudacion Acumulada]    Script Date: 7/6/2015 9:59:02 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[Recaudacion Acumulada]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Rpt].[Recaudacion Acumulada]
GO
/****** Object:  StoredProcedure [Rpt].[Recaudacion Acumulada]    Script Date: 7/6/2015 9:59:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[Recaudacion Acumulada]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



-- =============================================
-- Author:		Sebastien Dutot
-- Create date: 2015/05/08
-- Description:	Generate list of records for the Report Recaudacion Acumulada
-- draft
-- =============================================
CREATE PROCEDURE [Rpt].[Recaudacion Acumulada]
	-- Add the parameters for the stored procedure here
	@pStartDate datetime = NULL,
	@pEndDate datetime= NULL,
	--@IsActive nvarchar(max),
	--@CardStatus nvarchar(max),
	--@RUT nvarchar(50),
	@AccountNum nvarchar(200) = ''%'',
	@AccountType nvarchar(max),
	@Channel nvarchar(max),
	@PayType nvarchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		declare @StartDate int
	declare @EndDate int
		--declare @IsActiveIds table (item nvarchar(10))
	declare @AccountTypeIds table (item nvarchar(50))
	declare @ChannelIds table (item nvarchar(50))
	declare @PayTypeIds table (item nvarchar(50))
	--select @StartDate = date_key from ITS_ODS.dim.Date
	--where date.Date = isnull(@pStartDate,convert(date,dateadd(day,-30,getdate())))

	--		select @EndDate  = date_key from ITS_ODS.dim.Date
	--where date.Date = isnull(@pEndDate,convert(date,dateadd(day,30,getdate())))
	select @pStartDate= convert(date,coalesce(@pStartDate,convert(date,dateadd(day,-30,getdate()))))

			select @pEndDate  =convert(date,coalesce(@pEndDate,convert(date,dateadd(day,30,getdate()))))


	--insert into  @IsActiveIds(Item) (select Item from dbo.ufnSplit(@IsActive,'',''))

	--insert @HighwayIds(Item) value (select (coalesce(cast(@Highwayid as nvarchar),(select Item from dbo.ufnSplit(@Highway,'',''))))
	--select @TollpointIds= coalesce(cast(@tollPointId as nvarchar),(select Item from dbo.ufnSplit(@tollPoint,'','')))

	insert into  @AccountTypeIds(Item) (select Item from dbo.ufnSplit(@AccountType,'',''))
	insert into  @ChannelIds(Item) (select Item from dbo.ufnSplit(@Channel,'',''))
	insert into  @PayTypeIds(Item) (select Item from dbo.ufnSplit(@PayType,'',''))

SELECT	
	CC.NID as RUT,
	CA.AccountNumber, 
--case when CC.CompanyName Is NULL then 
--CC.FirstName + '' '' + CC.LastName 
--else cc.CompanyName  end as ''Nombre o Razon Social''	,
--ACT.AcctTypeDesc,
coalesce(ACT.Translations.value(''(/rootnode/data[@name="es-cl"])[1]'',''nvarchar(50)''),ACT.AcctTypeDesc) as AcctTypeDesc,
FM.[FJDate] AS [Date],
		FP.FJNo AS [FJNo],
		CASE ISNULL(FP.PayType,'''')
			WHEN ''CASH'' THEN ''EFECTIVO''--''CASH''
			WHEN ''ECHECK'' THEN ''DÉBITO DIRECTO''--''DIRECT DEBIT''
			WHEN ''CHECK'' THEN ''CHEQUE''
			WHEN ''CHEQUE'' THEN ''CHEQUE''
			WHEN ''CREDIT'' THEN ''CRÉDITO''--''CREDIT''
			WHEN ''SRVIPAG'' THEN (case spr.MeanOfPayment when ''CH'' THEN ''CHEQUE'' WHEN ''IF'' THEN ''EFECTIVO''--''CASH''
			 ELSE ''SRVIPAG'' END)
			ELSE  '''' END + CASE 
			RTRIM(UPPER(FP.PayType))
			WHEN ''CASH'' THEN ''''
			WHEN ''ECHECK'' THEN '' '' + ISNULL(FP.ECheckAcctNo,'''')
			WHEN ''CHEQUE'' THEN '' '' + ISNULL(FP.CheckNumber,'''')
			WHEN ''CREDIT'' THEN '' '' + ISNULL(FP.CCType,'''') + '' '' + ISNULL(FP.CCnumber,'''')
			ELSE  ''''
			END  AS [Pay Type],
			CASE ISNULL(FP.PayType,'''')
			WHEN ''CASH'' THEN ''EFECTIVO''--''CASH''
			WHEN ''ECHECK'' THEN ''DÉBITO DIRECTO''--''DIRECT DEBIT''
			WHEN ''CHECK'' THEN ''CHEQUE''
			WHEN ''CHEQUE'' THEN ''CHEQUE''
			WHEN ''CREDIT'' THEN ''CRÉDITO''--''CREDIT''
			WHEN ''SRVIPAG'' THEN (case spr.MeanOfPayment when ''CH'' THEN ''CHEQUE'' WHEN ''IF'' THEN ''EFECTIVO''--''CASH''
			 ELSE ''SRVIPAG'' END)
			ELSE  '''' END as ''PayTypeShort'',
		CAST(FP.FinPayAmt AS MONEY) AS Amount,
		--ISNULL(FMM.ModDisplayName,'''') AS [Payment Description],
		--fmm.ModProgramName	,
		--fmm.ModName,
		
--pre.PayRequestType,
--pre.AppType,
case 
WHEN pre.Comments is null and fp.PayType=''SRVIPAG'' THEN ''SRVIPAG_''+spft.[Description]
when pre.Comments IS NULL  and fp.paytype in (''CASH'',''CHECK'',''CHEQUE'') then ''Pago Manual via TollCRM''
WHEN pre.Comments =''||||'' then ''Website''
ELSE pre.Comments END as Payment_Channel,
fp.CreatedBy
FROM    
	Financial.FinMaster FM 
	INNER JOIN Financial.FinPayment FP ON FM.FJNo = FP.FJNo INNER JOIN
	Financial.FinTransMaster FTM ON FM.FJNo = FTM.FjNo INNER JOIN
	Financial.FinModuleMst FMM ON FP.LastUpdModuleID = FMM.ModuleId 
	left join Servipag.ServipagReturn spr on fm.FJNo=spr.FJNo
	left join Servipag.ServipagFile spf on spf.ServipagFileId=spr.FileId
	left join Servipag.ServipagFileType spft on spft.ServipagFileTypeId=spf.ServipagFileTypeID
	LEFT OUTER JOIN
	CSCPay.PayResponse PR ON FTM.FjNo = PR.FJNo
	left  join CSCPay.PayRequest pre on pr.PayRequestId=pre.PayRequestId 
	inner join Customer.Account CA on coalesce(Pr.AccountId,ftm.acctid) = CA.AccountId
	
inner join Customer.Contact CC on CA.AccountId = CC.AccountId
inner join Customer.AccountType ACT on CA.AccountTypeId=ACT.AccountTypeId
where convert(date,FM.[FJDate]) between @pStartDate and  @pEndDate
--  --and Card.isActive in (select item from @IsActiveIds) --(select Item from dbo.ufnSplit(@Highway,'',''))
--  and CardStatus.CardStatusId in (select item from  @CardStatusIds)--(select Item from dbo.ufnSplit(@TollPoint,'',''))
--order by card.created  desc
--where fp.PayType=''||||''
AND case 
WHEN pre.Comments is null and fp.PayType=''SRVIPAG'' THEN ''SRVIPAG_''+spft.[Description]
when pre.Comments IS NULL  and fp.paytype in (''CASH'',''CHECK'',''CHEQUE'') then ''Pago Manual via TollCRM''
WHEN pre.Comments =''||||'' then ''Website''
ELSE pre.Comments END  in (select item from  @ChannelIds)
AND 	CASE ISNULL(FP.PayType,'''')
			WHEN ''CASH'' THEN ''EFECTIVO''--''CASH''
			WHEN ''ECHECK'' THEN ''DÉBITO DIRECTO''--''DIRECT DEBIT''
			WHEN ''CHECK'' THEN ''CHEQUE''
			WHEN ''CHEQUE'' THEN ''CHEQUE''
			WHEN ''CREDIT'' THEN ''CRÉDITO''--''CREDIT''
			WHEN ''SRVIPAG'' THEN (case spr.MeanOfPayment when ''CH'' THEN ''CHEQUE'' WHEN ''IF'' THEN ''EFECTIVO''--''CASH''
			 ELSE ''SRVIPAG'' END)
			ELSE  '''' END  in (select item from  @PayTypeIds)
and ACT.AccountTypeId in (select item from  @AccountTypeIds)
and CA.AccountNumber like coalesce(@AccountNum,''%'')
order by fm.FJDate desc
END






' 
END
GO
