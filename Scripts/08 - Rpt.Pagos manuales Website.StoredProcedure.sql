USE [ITS_CSC]
GO
/****** Object:  StoredProcedure [Rpt].[Pagos manuales Website]    Script Date: 7/6/2015 9:59:02 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[Pagos manuales Website]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Rpt].[Pagos manuales Website]
GO
/****** Object:  StoredProcedure [Rpt].[Pagos manuales Website]    Script Date: 7/6/2015 9:59:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[Pagos manuales Website]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'




-- =============================================
-- Author:		Sebastien Dutot
-- Create date: 2015/05/12
-- Description:	Generate list of manual payments processed by the system made by Website 
-- =============================================
CREATE PROCEDURE [Rpt].[Pagos manuales Website]
	-- Add the parameters for the stored procedure here
	@pStartDate datetime = NULL,
	@pEndDate datetime= NULL
	--@IsActive nvarchar(max),
	--@CardStatus nvarchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		declare @StartDate int
	declare @EndDate int
		--declare @IsActiveIds table (item nvarchar(10))


	--select @StartDate = date_key from ITS_ODS.dim.Date
	--where date.Date = isnull(@pStartDate,convert(date,dateadd(day,-30,getdate())))

	--		select @EndDate  = date_key from ITS_ODS.dim.Date
	--where date.Date = isnull(@pEndDate,convert(date,dateadd(day,30,getdate())))
	select @pStartDate= convert(date,isnull(@pStartDate,convert(date,(select MIN(FJDate) from Financial.FinMaster))))

			select @pEndDate  =convert(date,isnull(@pEndDate,convert(date,(select MAX(FJDate) from Financial.FinMaster))))




SELECT	
	CA.AccountNumber, 
case when CC.CompanyName Is NULL then 
CC.FirstName + '' '' + CC.LastName 
else cc.CompanyName  end as ''Nombre o Razon Social''	,
FM.[FJDate] AS [Date],
		FP.FJNo AS [FJNo],
		CASE ISNULL(FP.PayType,'''')
			WHEN ''CASH'' THEN ''EFECTIVO''--''CASH''
			WHEN ''ECHECK'' THEN ''DÉBITO DIRECTO''--''DIRECT DEBIT''
			WHEN ''CHECK'' THEN ''CHEQUE''
			WHEN ''CHEQUE'' THEN ''CHEQUE''
			WHEN ''CREDIT'' THEN ''CRÉDITO''--''CREDIT''
			ELSE  '''' END + CASE 
			RTRIM(UPPER(FP.PayType))
			WHEN ''CASH'' THEN ''''
			WHEN ''ECHECK'' THEN '' '' + ISNULL(FP.ECheckAcctNo,'''')
			WHEN ''CHEQUE'' THEN '' '' + ISNULL(FP.CheckNumber,'''')
			WHEN ''CREDIT'' THEN '' '' + ISNULL(FP.CCType,'''') + '' '' + ISNULL(FP.CCnumber,'''')
			ELSE  ''''
			END  AS [Pay Type],
		CAST(FP.FinPayAmt AS MONEY) AS Amount,
		--ISNULL(FMM.ModDisplayName,'''') AS [Payment Description],
		--fmm.moduleType	,
		
pre.PayRequestType,
pre.AppType,
pre.Comments,
fp.CreatedBy,
pre.ClientIPAddress
FROM    
	Financial.FinMaster FM 
	INNER JOIN Financial.FinPayment FP ON FM.FJNo = FP.FJNo INNER JOIN
	Financial.FinTransMaster FTM ON FM.FJNo = FTM.FjNo INNER JOIN
	--Financial.FinModuleMst FMM ON FP.LastUpdModuleID = FMM.ModuleId LEFT OUTER JOIN
	CSCPay.PayResponse PR ON FTM.FjNo = PR.FJNo
	left outer join CSCPay.PayRequest pre on pr.PayRequestId=pre.PayRequestId 
	inner join ITS_CSC.Customer.Account CA on coalesce(Pr.AccountId,ftm.acctid) = CA.AccountId
inner join ITS_CSC.Customer.Contact CC on CA.AccountId = CC.AccountId

	where convert(date,FM.[FJDate]) between @pStartDate and  @pEndDate
	and pre.PayRequestType = 2
	and pre.Comments=''||''
	--and pre.PayRequestType = 2
	--or 
	--FP.PayType in 
	--		( ''CASH'',
	--		''CHECK'',
	--		''CHEQUE'' )
--	 and pre.Comments --= ''||||''
--	not in 
--	(''Pago Manual via TollCRM'',
--''Rebill Process'')
order by FM.[FJDate]  asc


END







' 
END
GO
