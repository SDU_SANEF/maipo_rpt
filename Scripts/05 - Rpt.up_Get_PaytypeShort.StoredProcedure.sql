USE [ITS_CSC]
GO
/****** Object:  StoredProcedure [Rpt].[up_Get_PaytypeShort]    Script Date: 7/6/2015 9:59:02 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[up_Get_PaytypeShort]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Rpt].[up_Get_PaytypeShort]
GO
/****** Object:  StoredProcedure [Rpt].[up_Get_PaytypeShort]    Script Date: 7/6/2015 9:59:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[up_Get_PaytypeShort]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


-- =============================================
-- Author:		Dutot Sebastien
-- Create date: 2015-05-12
-- Description:	Return the list of PaytypeShort Description by concatenation
-- =============================================
CREATE PROCEDURE [Rpt].[up_Get_PaytypeShort]
	-- Add the parameters for the stored procedure here
	@PaytypeShortIDs nvarchar(max)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	DECLARe @PaytypeShortDesc nvarchar(max)
	DECLARe @allPaytypeShortDesc nvarchar(max);
with t1 as(	select 		distinct CASE ISNULL(FP.PayType,'''')
			WHEN ''CASH'' THEN ''EFECTIVO''--''CASH''
			WHEN ''ECHECK'' THEN ''DÉBITO DIRECTO''--''DIRECT DEBIT''
			WHEN ''CHECK'' THEN ''CHEQUE''
			WHEN ''CHEQUE'' THEN ''CHEQUE''
			WHEN ''CREDIT'' THEN ''CRÉDITO''--''CREDIT''
			WHEN ''SRVIPAG'' THEN (case spr.MeanOfPayment when ''CH'' THEN ''CHEQUE'' WHEN ''IF'' THEN ''EFECTIVO''--''CASH''
			 ELSE ''SRVIPAG'' END)
			ELSE  '''' END as PayTypeShort
			from 	--Financial.FinMaster FM 
	--INNER JOIN
	 Financial.FinPayment FP --ON FM.FJNo = FP.FJNo INNER JOIN
	--Financial.FinTransMaster FTM ON FM.FJNo = FTM.FjNo INNER JOIN
	--Financial.FinModuleMst FMM ON FP.LastUpdModuleID = FMM.ModuleId 
	left join Servipag.ServipagReturn spr on FP.FJNo=spr.FJNo)

	select @PaytypeShortDesc= 
	 STUFF( (SELECT '','' + PaytypeShort from t1
where PaytypeShort in (select Item from dbo.ufnSplit(@PaytypeShortIDs,'',''))
                             FOR XML PATH('''')), 
                            1, 1, '''')
	, @allPaytypeShortDesc= 
	--select
	STUFF( (SELECT '','' + PaytypeShort
	--CardStatusDesc
from t1
                             FOR XML PATH('''')), 
                            1, 1, '''')

IF @PaytypeShortDesc=@allPaytypeShortDesc
begin 
select ''TODO'' as PaytypeShort_Desc
end
else
begin
select coalesce(@PaytypeShortDesc,''N/A'') as PaytypeShort_Desc
end	
END




' 
END
GO
