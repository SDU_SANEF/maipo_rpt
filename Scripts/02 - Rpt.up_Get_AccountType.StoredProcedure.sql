USE [ITS_CSC]
GO
/****** Object:  StoredProcedure [Rpt].[up_Get_AccountType]    Script Date: 7/6/2015 9:59:02 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[up_Get_AccountType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Rpt].[up_Get_AccountType]
GO
/****** Object:  StoredProcedure [Rpt].[up_Get_AccountType]    Script Date: 7/6/2015 9:59:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[up_Get_AccountType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Dutot Sebastien
-- Create date: 2015-05-12
-- Description:	Return the list of Account type Description by concatenation
-- =============================================
CREATE PROCEDURE [Rpt].[up_Get_AccountType]
	-- Add the parameters for the stored procedure here
	@AccountTypeIDs nvarchar(max)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	DECLARe @AccountTypeDesc nvarchar(max)
	DECLARe @allAccountTypeDesc nvarchar(max)


	select @AccountTypeDesc= 
	--select
	STUFF( (SELECT '','' + coalesce(Translations.value(''(/rootnode/data[@name="es-cl"])[1]'',''nvarchar(50)''),[AcctTypeDesc])
from [Customer].[AccountType]
where AccountTypeId in (select Item from dbo.ufnSplit(@AccountTypeIDs,'',''))
                             FOR XML PATH('''')), 
                            1, 1, '''')
	select  @allAccountTypeDesc= 
	--select
	STUFF( (SELECT '','' + coalesce(Translations.value(''(/rootnode/data[@name="es-cl"])[1]'',''nvarchar(50)''),[AcctTypeDesc])
	--CardStatusDesc
from [Customer].[AccountType]
                             FOR XML PATH('''')), 
                            1, 1, '''')
IF @AccountTypeDesc=@allAccountTypeDesc
begin 
select ''TODO'' as AccountType_Desc
end
else
begin
select coalesce(@AccountTypeDesc,''N/A'') as AccountType_Desc
end	
END



' 
END
GO
