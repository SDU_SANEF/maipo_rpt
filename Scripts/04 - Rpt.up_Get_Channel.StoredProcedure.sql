USE [ITS_CSC]
GO
/****** Object:  StoredProcedure [Rpt].[up_Get_Channel]    Script Date: 7/6/2015 9:59:02 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[up_Get_Channel]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Rpt].[up_Get_Channel]
GO
/****** Object:  StoredProcedure [Rpt].[up_Get_Channel]    Script Date: 7/6/2015 9:59:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Rpt].[up_Get_Channel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Dutot Sebastien
-- Create date: 2015-05-12
-- Description:	Return the list of Channel Description by concatenation
-- =============================================
CREATE PROCEDURE [Rpt].[up_Get_Channel]
	-- Add the parameters for the stored procedure here
	@ChannelIDs nvarchar(max)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	DECLARe @ChannelDesc nvarchar(max)
	DECLARe @allChannelDesc nvarchar(max);
with t1 as(
SELECT ''Pago Manual via TollCRM'' as ChannelID,  ''Pago Manual via TollCRM'' as Channel
UNION
SELECT  ''Rebill Process'' as ChannelID, ''Rebill Process'' as Channel
UNION
--SELECT ''SRVIPAG''  as ChannelID,''SRVIPAG''  as Channel
--UNION
SELECT  ''||||'' as ChannelID,  ''Website'' as Channel
UNION

select distinct ''SRVIPAG_''+description as ChannelID,''SRVIPAG_''+description as Channel
from Servipag.ServipagFileType)

	select @ChannelDesc= 
	 STUFF( (SELECT '','' + Channel from t1
where ChannelID in (select Item from dbo.ufnSplit(@ChannelIDs,'',''))
                             FOR XML PATH('''')), 
                            1, 1, '''')
	, @allChannelDesc= 
	--select
	STUFF( (SELECT '','' + [Channel]
	--CardStatusDesc
from t1
                             FOR XML PATH('''')), 
                            1, 1, '''')

IF @ChannelDesc=@allChannelDesc
begin 
select ''TODO'' as Channel_Desc
end
else
begin
select coalesce(@ChannelDesc,''N/A'') as Channel_Desc
end	
END



' 
END
GO
